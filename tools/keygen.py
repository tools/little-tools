#!/usr/bin/env python3

"""
	CopyLeft 2019 Pascal Engélibert

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

__version__ = "1.0"

import sys
from duniterpy.key import SigningKey

def getargv(arg, default=""):
	if arg in sys.argv and len(sys.argv) > sys.argv.index(arg)+1:
		return sys.argv[sys.argv.index(arg)+1]
	else:
		return default

if __name__ == "__main__":
	
	if "--help" in sys.argv or not "-i" in sys.argv or not "-o" in sys.argv:
		print("""Duniter MiniTools: Mass Keygen
CopyLeft 2019 Pascal Engélibert

Transform a TSV [psw1,psw2] into a TSV [psw1,psw2,pubkey].

Usage:
python3 keygen.py -i <input_file> -o <output_file>
Format: TSV ; Separator: tab

Options: --help, --version
""")
		exit()
	
	if "--version" in sys.argv:
		print(__version__)
		exit()
	
	infile = open(getargv("-i"), "r")
	outfile = open(getargv("-o"), "w")
	
	i = 0
	while True:
		line = infile.readline()
		if line == "":
			break
		i += 1
		if "\n" in line:
			line = line[:len(line)-1]
		
		cols = line.split("\t")
		while "" in cols:
			cols.remove("")
		if len(cols) < 2:
			print("Error (line "+str(i)+"): less than 2 columns! (at least 2 expected)")
			break
		
		key = SigningKey.from_credentials(cols[0], cols[1])
		outfile.write(cols[0]+"\t"+cols[1]+"\t"+key.pubkey+"\n")
	
	infile.close()
	outfile.close()
